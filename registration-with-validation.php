<?php

$output = "";

if(isset($_POST['username'])){

    $username = strtolower($_POST['username']);  

    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

    $dob = $_POST['dob'];

    $message = preg_replace("/[^a-zA-Z0-9 ]/", "", $_POST['message']);  





    $birthDate = new DateTime($dob);

    $today = new DateTime('today');

    $age = $today->diff($birthDate)->y;





    $output = "<h2>Registration Details</h2>";

    $output .= "<p>Username: $username</p>";





    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

    $output .= "<p class='error'>Email: Invalid format (Ex: some.string@domain.com)</p>";

    } else {

    $output .= "<p>Email: $email</p>";

    }



    $output .= "<p>Date of Birth: $dob</p>";

    $output .= "<p>";

    if ($age < 18) {

    $output .= "You are a minor.";

    } else {

    $output .= "You are not a minor.";

    }

    $output .= "</p>";



    $output .= "<p>Message: $message</p>";

}



?>



<!DOCTYPE html>

<html>

<head>

<title>Registration Result</title>

<style>

  .invalid {

    border-color: red;

  }

    body {

  margin: auto;

  width: 25%;

  border: 3px solid green;

  padding: 10px;

}

	h1{

		color: green;

	}

.error {

    color: red;

  }



</style>

</head>

<body>

  <a href="javascript:history.go(-1)">Go back</a>

<?php echo $output; ?>



<!-- output string empty -->

<?php if($output == "") { ?>

    <h1>Registration Form</h1>



    <form action="" method="post">

    <label for="username">Username:</label>

    <input type="text" id="username" name="username" required><br><br>

    

    <label for="email">Email:</label>

    <input type="text" id="email" name="email" onkeyup="handleEmailInput()" required><br><br>

    

    <label for="dob">Date of Birth:</label>

    <input type="date" id="dob" name="dob" max="<?php echo date("Y-m-d"); ?>" required><br><br>

    

    <label for="message">Message:</label>

    <textarea id="message" name="message" required></textarea><br><br>

    

    <input type="submit" value="Register">

    </form>





    <script>

    function validateEmail(email) {

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);

    }



    function handleEmailInput() {

        const emailInput = document.getElementById("email");

        const email = emailInput.value;

        

        if (!validateEmail(email)) {

        emailInput.classList.add("invalid");

        } else {

        emailInput.classList.remove("invalid");

        }

    }

    </script>

<?php } ?>

</body>

</html>

